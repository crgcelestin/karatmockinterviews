# Arrays and Lists
- [Shuffle the Array](https://leetcode.com/problems/shuffle-the-array/description/)
- [Maximum Subarray](https://leetcode.com/problems/maximum-subarray/description/)
- [Defuse the Bomb](https://leetcode.com/problems/maximum-subarray/description/)
- [Pascal's Triangle](https://leetcode.com/problems/pascals-triangle/description/)
- [PT II](https://leetcode.com/problems/pascals-triangle-ii/description/)
- [Can Place Flowers](https://leetcode.com/problems/can-place-flowers/description/)
- [Matrix Reshape](https://leetcode.com/problems/reshape-the-matrix/description/)
- [Product of Array Except Self](https://leetcode.com/problems/product-of-array-except-self/description/)

# Strings
- [Valid Anagram](https://leetcode.com/problems/valid-anagram/)
- [Rev String](https://leetcode.com/problems/reverse-string/)
- [Longest Palindrome](https://leetcode.com/problems/longest-palindrome/)
- [Valid Palindrome](https://leetcode.com/problems/valid-palindrome/)
- [Implement strStr()](https://leetcode.com/problems/implement-strstr/)
- [Count & Say](https://leetcode.com/problems/count-and-say/)
- [Rev Words in String](https://leetcode.com/problems/reverse-words-in-a-string/)

# Sliding Window Technique
- [longest sub w/o repeating chars](https://leetcode.com/problems/longest-substring-without-repeating-characters/)
- [min swap](https://leetcode.com/problems/minimum-swaps-to-group-all-1s-together-ii/)
- [min size subarray sum](https://leetcode.com/problems/minimum-size-subarray-sum/)
- [min window substring](https://leetcode.com/problems/minimum-window-substring/)

# O(n*log(n)) Sorting Techniques
- [sort array](https://leetcode.com/problems/sort-an-array)
