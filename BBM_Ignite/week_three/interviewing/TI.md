# Wk 2 - Technical
- Key Communication Elements
    * Listen and Adapt: Hear hints and feedback and adapt strategy accordingly
    * Ask for Clarification
        - if unsure ask for clarity
            * Could you elaborate on this problem aspect?
            * Do I understand that teh main requirement is?
        - propose test cases/expected outputs
            - For clarity if input is 'x' then output would be 'y'?
    * Comms - Verbalize thought process
- Online Assessments
    - Allocation proper time per question
    - leetcode, etc.
    - start with questions i'm confident in
    - simple solution = priority
