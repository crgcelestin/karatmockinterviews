# Stacks and Qs
- [valid parens](https://leetcode.com/problems/valid-parentheses/)
- [implement stack w/ queues](https://leetcode.com/problems/implement-stack-using-queues/)
- [queue w/ stacks](https://leetcode.com/problems/implement-queue-using-stacks/)
- [lru cache](https://leetcode.com/problems/lru-cache/)
- [circular q](https://leetcode.com/problems/design-circular-queue/)
- [min stack](https://leetcode.com/problems/min-stack)
- [trap rain](https://leetcode.com/problems/trapping-rain-water)
- [largest histogram rect](https://leetcode.com/problems/largest-rectangle-in-histogram)

# Recursion
- Priority
    - [fib #](https://leetcode.com/problems/fibonacci-number/)
    - [subsets](https://leetcode.com/problems/subsets/)
    - [gen parens](https://leetcode.com/problems/generate-parentheses/)
    - [sudoku solver](https://leetcode.com/problems/sudoku-solver/)
- Extra
    - [pow(x,n)](https://leetcode.com/problems/powx-n/)
    - [permutations](https://leetcode.com/problems/permutations/)
    - [combo sum](https://leetcode.com/problems/combination-sum/)
    - [word search](https://leetcode.com/problems/word-search/)
    - [n-queens](https://leetcode.com/problems/n-queens/)
