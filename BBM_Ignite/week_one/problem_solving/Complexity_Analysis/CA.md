# [Complexity Analysis Source](https://www.geeksforgeeks.org/practice-questions-time-complexity-analysis/)

1.
```python
a = 0
b = 0
for i in range(N):
  a = a + random()

for i in range(M):
  b= b + random()

'''
sc is O(1) and t(n) is O(n+m)
- No leading term as they are both independent variables irrespective of each other -> O(N+M), variable has no dependence on input size
'''
```

2.
```python
a = 0;
for i in range(N):
  for j in reversed(range(i,N)):
    a = a + i + j;

'''
sc is O(1) and tc is O(N*N)
'''
```

3.
```python
k = 0;
for i in range(n//2,n):
  for j in range(2,n,pow(2,j)):
        k = k + n / 2;

'''
tc is O(n*2**j)-> O(2**j -> n(2**j)) -> n * log(n)
j keeps doubling until equal to n -> several iterations of doubling till j is less than n would be to log(n)
second loop rns for O(log n) steps
and i runs for n/2 steps
so O(n * logn)
'''
```

4.
* If algo X is asymptotically more efficient than Y, then it means  X will be a better choice for large inputs
    - X essentially takes shorter steps as relative to Y when input grows to infinity

5.
```python
a = 0
i = N
while (i > 0):
  a += i
  i //= 2

'''
tc is O(log n)
as i is 1/2 each time, 1/2(i) -> 1/4(i) -> 1/8(i)
so iterations can be expressed as N*(1/2)^k = 1
where solving for k results in N = 2^k -> log2(n) = k
'''
```

6. Measuring na algo based on time and memory (tc, sc) is critical to compare algo efficiency
- An algo that takes less time, memory for larger input sizes is a far more efficient algo

7. Complexity is measured by counting the number of primitive ops performed by algo given input size

8. What is the tc?
```python
# code
for i in range(n):
    modified_i = i * k

'''
operations keep multiplying i by k, so i*k -> i*k**2 -> i*k**3 with an upper bound of n
so N*i**k = 1 -> N = k**i -> logk(n) = i
loop can k^c-1 where c = number of times i is to be multiplied by k until i reaches n -> K^c-1 = n -> log k (n)
'''
```
9.
```python
value = 0;
for i in range(n):
  for j in range(i):
    value=value+1

'''
the corresponding tc is O( n*(n-1)/2 )
- first loop runs n times, second runs n-1 times so n*n-1 -> n**2 - n
'''
```
10.
Algo A has tc of O(n) and algo B has tc of O(logn) -> algo B runs faster than A?
False as algo A may run faster than B for smaller numbers/input
