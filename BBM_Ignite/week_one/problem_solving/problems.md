# 5 Step Strategy to Solve Programming Problems
## (1) Understand Question and Repeat to Verify Question
## (2) Discuss initial ideas + Brainstorming
## (3) Approach + Formulate Algorithm
## (4) Code
## (5) Test Code w/ various Test Cases

# Problems List
## Linked List
- [Remove elements from LL by Index](https://www.freecodecamp.org/learn/coding-interview-prep/data-structures/remove-elements-from-a-linked-list-by-index)
- [Add elements at specific index in LL](https://www.freecodecamp.org/learn/coding-interview-prep/data-structures/add-elements-at-a-specific-index-in-a-linked-list)
- [Rev LL](https://leetcode.com/problems/reverse-linked-list/description/)
- [Merge 2 Sorted Lists](https://brilliantblackminds.thinkific.com/courses/take/ignite-week-1/texts/52098762-linked-lists#:~:text=Merge%20Twp%20Sorted%20Lists)
- [Remove Nth Node From List End](https://leetcode.com/problems/remove-nth-node-from-end-of-list/)
- [Linked List Cycle](https://leetcode.com/problems/linked-list-cycle/)
- [Palindrome Linked List](https://leetcode.com/problems/palindrome-linked-list/)
- [2 LLs Intersection](https://leetcode.com/problems/intersection-of-two-linked-lists/)
## Hash Map
- [Contains Duplicate 2](https://leetcode.com/problems/contains-duplicate-ii/)
- [Unique Morse Code Words](https://leetcode.com/problems/unique-morse-code-words/)
- [Top K Frequent Elements](https://leetcode.com/problems/top-k-frequent-elements/)
- [1st Missing +#](https://leetcode.com/problems/first-missing-positive/)
## Complexity Analysis
- [Longest Substring w/o repeated chars](https://leetcode.com/problems/longest-substring-without-repeating-characters/)
- [Min Swap Grouping 1s Together](https://leetcode.com/problems/minimum-swaps-to-group-all-1s-together-ii/)
- !! [Practice Qs](https://www.geeksforgeeks.org/practice-questions-time-complexity-analysis/) !!
