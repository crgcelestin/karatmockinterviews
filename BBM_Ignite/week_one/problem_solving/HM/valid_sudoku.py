"""
Determine if 9*9 Sudoku board is valid -> (1) each row has digits 1-9 w/o dup, (2) col has 1-9 w/o dups, (3) each of 3*3 sub boxes contain 1-9 without dups

- initialize flag that indicates if board is true or not
- initialize method checking for dups in each conditions either row, col, sub box
    * take in input of nums being a list of characters -> condense into new list that is only numbers not periods and ask if the length of input is equal to condensed input
        * false if they are not the same, i.e there are duplicates

- three methods to check row, col, boxes
    * first is row checking: for row in board, if not method(row), set flag to false
    * first is col checking: for col in zip(*board) [aligned cols], if not method(col), set flag to false
    * then sub boxes, iterate in units of 3*3 [3 cells in row by 3 cells in col] for 9*9 space, i.e two loops where the outer is rows [ for r in range(0.9,3), for c in range(0,9,3)], we now have a box
        Box defined by [
            board[i][j] // current box
            for i in range(r, r+3) // 0,1,3...7,8,9
            for j in range(c, c+3) // 0,1,3...7,8,9
        ]
        if not method(box), flag to false
    is_valid return
"""


class Solution:
    def isValidSudoku(self, board: list[list[str]]) -> bool:
        is_valid = True

        def is_duplicates(nums: list[str]) -> bool:
            nums = [num for num in nums if num != "."]
            return len(nums) == len(set(nums))

        for row in board:
            if not is_duplicates(row):
                is_valid = False
        for col in zip(*board):
            if not is_duplicates(col):
                is_valid = False
        for r in range(0, 9, 3):
            for c in range(0, 9, 3):
                box = [board[i][j] for i in range(r, r + 3) for j in range(c, c + 3)]
                if not is_duplicates(box):
                    is_valid = False
        return is_valid
