function LinkedList() {
    var length = 0;
    var head = null;

    var Node = function (element) {
        this.element = element;
        this.next = null;
    };

    this.size = function () {
        return length;
    };

    this.head = function () {
        return head;
    };

    this.add = function (element) {
        var node = new Node(element);
        if (head === null) {
            head = node;
        } else {
            var currentNode = head;

            while (currentNode.next) {
                currentNode = currentNode.next;
            }

            currentNode.next = node;
        }

        length++;
    };

    this.remove = function (element) {
        var currentNode = head;
        var previousNode;
        if (currentNode.element === element) {
            head = currentNode.next;
        } else {
            while (currentNode.element !== element) {
                previousNode = currentNode;
                currentNode = currentNode.next;
            }

            previousNode.next = currentNode.next;
        }

        length--;
    };
    this.isEmpty = () => {
        // edit
        return this.size() > 0 ? false : true;
    }
    // Only change code below this line
    this.indexOf = function (el) {
        let currentNode = head, index = -1, notFound = false;
        while (currentNode && !notFound) {
            index++
            if (currentNode.element === el) {
                notFound = true
            }
            currentNode = currentNode.next
        }
        return notFound ? index : -1;
    }
    this.elementAt = function (i) {
        let currentNode = head, currentElement, eleFound = false, index = -1;
        while (currentNode && !eleFound) {
            index++;
            currentElement = currentNode.element;
            if (i === index) {
                eleFound = true;
            }
            currentNode = currentNode.next;
        }
        return eleFound ? currentElement : undefined;
    }
    // Only change code above this line
}
