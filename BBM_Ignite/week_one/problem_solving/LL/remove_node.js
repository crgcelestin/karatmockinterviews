function LinkedList() {
    var length = 0;
    var head = null;

    var Node = function (element) {
        this.element = element;
        this.next = null;
    };

    this.size = function () {
        return length;
    };

    this.head = function () {
        return head;
    };

    this.add = function (element) {
        let newNode = new Node(element);
        // no Nodes
        if (head === null) {
            head = newNode;
            // 1+ Nodes
        } else {
            let currentNode = head;
            // iterate if we a new node after current
            while (currentNode.next) {
                currentNode = currentNode.next;
            }
            currentNode.next = newNode;
        }

        length++;
    };

    this.remove = function (element) {
        let current = head
        if (current === null) {
            return
        } else {
            // find that first node is target, set head to next node or null, decrement
            if (current.element === element) {
                head = current.next
                // edit 1
                return length--;
            }
            /*
            iterating for search
                store LL in previous var
                while we are at a non-null element in LL,
                    set current to next node,
                    if that is non-null
                        if we find it is target
                            then skip i.e set Node's next in previous to be next node so skip over element that is target
                            decrement
                set var of LL to next Node held by current to iterate
            */
            let previous = head
            while (previous) {
                let current = previous.next
                if (current) {
                    if (current.element === element) {
                        previous.next = current.next
                        return length--;
                    }
                }
                previous = current
                // first case is if there is one node and its not the target search
                // second case if there is one node and its the target search
                // multiple nodes in LL, iterate through LL to find target node
                // sub 1: we find target node, remove if not first
            }
        }
    }
}
