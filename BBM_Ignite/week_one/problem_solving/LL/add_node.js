function LinkedList() {
    // initialize length property starting at 0 length, no head set it to None/null
    let length = 0;
    let head = null;

    // Node class to be comps of LL containing element, next pointer
    function Node(element) {
        this.element = element;
        this.next = null;
    };

    this.head = () => head;

    this.size = () => length;

    /*
        add method -
            initialize new Node with element to be added
            if we have node already we need to iterate through list in order to know where we should add Node
                starting at head
                store current element (head) in current var
                    iterate by asking if next node is null, if not null then next Node
                we get to end i.e where next points at null, set NewNode to next Node
            Find node with no next node, then add if node points to null

            else we have an empty LL, initialize newNode as head
            increment length property
    */
    this.add = function (element) {
        let newNode = new Node(element)
        if (head) {
            let current = head
            while (current.next !== null) {
                current = current.next
            }
            current.next = newNode
        } else {
            head = newNode;
        }
        length += 1;
    };
}
