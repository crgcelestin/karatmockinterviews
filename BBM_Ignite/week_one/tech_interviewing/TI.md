# BBM - Ignite Course - Wk 1
tc, sc = time, space complexity
## 5 Steps to Solving Engineering Problems
1. Understand Question + Repeat to Verify
- Identify and define problem
- Ask clarifying questions
- identify outputs, inputs

2. Discuss initial ideas + Brainstorming
- brainstorm approaches (pros, cons)
- consider tc, sc
    - explore various algos/data structures relevant to problem
- in interview
    - communicate approaches with interviewer
    - write down initial thoughts

3. Approach + Formulate Algo
- select ideal approach
- formulate algo (step by step)
    * entire problem -> smaller subproblems
- identify requirements and function signature
- consider tc, sc of chosen approach
- visualize algo with test case attempts

4. Code
- good variable, function names
- focus on code modularity + reusability

5. Test Code with Several Test Cases
- test with sample case + consider other edge cases
- any issues: id root cause, determine proper course of action

__Technical Interview Types__
- Behavioral
    - Comms, Problem Solving, Adaptability [ STAR Method - Past Experiences]
    - Knowledge
        - CS Fundamentals, languages, tech domains [ explain concepts, principles ]
- Coding
    - Leetcode, HackerRank
- System Design
    - Scalable, efficient, robust systems
        - Basic design principles, architecture, scalability
- Take Home
    - Real world scenarios [ clean, efficient, well doc'ed code ]

## Tech Interviewing
- Preparing
    * Focus on mental prep, dev strategies, understand what interviewer is looking for
        - Interview: Platform to demo, skills, architecture, achievements, projects
        - Growth: increased resilience, adaptability

- Interviewer's Perspective
    * Tech Competence
        - Strong skills align with role
        - Proficiency in particular programming languages, algo and data structure understanding, familiar with relevant tools and tech
        - Problem solving ability
            * methodology, eval steps -> get to final answer
    * Culture Fit
        * communication, organization alignment

- __PRACTICE__
    * 1hr daily to practice challenges
    * conduct assessment of skills -> id strengths and weaknesses [ targeted practice ]
    * Focus on complex algos, sys designs, languages
    * Mock interview

- __RESUME__
    - Structure: clear sections [content, expertise, education]
    - Action verbs, relevant experience, quantify achievements
