# __MAY 29TH__
- [My Solution](solution.py)
- [GPT Aided Solution](alt_solution.py)

## Problem Description
You are with your friends in a castle, where there are multiple rooms named after flowers. Some of the rooms contain treasures - we call them the treasure rooms. Each room contains a single instruction that tells you which room to go to next.

```js
 *** instructions_1 ***

 lily ---------     daisy  sunflower
               |       |     |
               v       v     v
 jasmin ->  tulip      violet    -> rose --->
            ^    |      ^             ^     |
            |    |      |             |     |
            ------    iris            -------
```

```py
'''
This is given as a list of pairs of (source_room, destination_room)
'''

instructions_1 = [
    ["jasmin", "tulip"],
    ["lily", "tulip"],
    ["tulip", "tulip"],
    ["rose", "rose"],
    ["violet", "rose"],
    ["sunflower", "violet"],
    ["daisy", "violet"],
    ["iris", "violet"]
]
```

Write a function that takes two parameters as input:
* A list containing the treasure rooms, and
* A list of instructions represented as pairs of (source_room, destination_room)

and returns a collection of all the rooms that satisfy the following two conditions:

* At least two *other* rooms have instructions pointing to this room
* This room's instruction immediately points to a treasure room

```py
instructions_1 = [
    ["jasmin", "tulip"],
    ["lily", "tulip"],
    ["tulip", "tulip"],
    ["rose", "rose"],
    ["violet", "rose"],
    ["sunflower", "violet"],
    ["daisy", "violet"],
    ["iris", "violet"]
]

treasure_rooms_1 = ["lily", "tulip", "violet", "rose"]

filter_rooms(treasure_rooms_1, instructions_1) => ["tulip", "violet"]

'''
* tulip can be accessed from rooms lily and jasmin. Tulip's instruction points to a treasure room (tulip itself)
* violet can be accessed from daisy, sunflower and iris. Violet's instruction points to a treasure room (rose)
'''
```


Additional inputs

```py
treasure_rooms_2 = ["lily", "jasmin", "violet"]

instructions_1 = [
    ["jasmin", "tulip"],
    ["lily", "tulip"],
    ["tulip", "tulip"],
    ["rose", "rose"],
    ["violet", "rose"],
    ["sunflower", "violet"],
    ["daisy", "violet"],
    ["iris", "violet"]
]

filter_rooms(treasure_rooms_2, instructions_1) => []
 '''
 none of the rooms reachable from tulip or violet are treasure rooms
 '''
```

```js
 *** instructions_2 ***

 lily --------             ------
               |          |      |
               v          v      |
 jasmin ->  tulip -- > violet ---^

treasure_rooms_3 = ["violet"]

instructions_2 = [
    ["jasmin", "tulip"],
    ["lily", "tulip"],
    ["tulip", "violet"],
    ["violet", "violet"]
]

filter_rooms(treasure_rooms_3, instructions_2)    => [tulip]
```
* Tulip can be accessed from rooms lily and jasmin. Tulip's instruction points to a treasure room (violet)

Complexity Analysis variables:
T: number of treasure rooms
I: number of instructions given

```py
# Initial thought process
violet, tulip, rose, jasmin
treasure_rooms = [rose, violet]

v -> t
r -> t
r -> v
t -> r


[a,b,d,c] <- violet
['tulip', 'violet']
['rose' , 'violet']
def filter_rooms (list of treasure rooms, instructions):
    match={}
    for i in range(instructions)
        if [1] === treasure room in array of subarrays, collect arrays(append)
        match1={
            'tulip': v,r
        }
        ^ instruction that to point to treasure room
        match2={
            'tulip':r
        }
```
