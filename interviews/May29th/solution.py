instructions_1 = [["jasmin", "tulip"], ["lily", "tulip"], ["tulip", "tulip"],
                  ["rose", "rose"], ["violet", "rose"],
                  ["sunflower", "violet"], ["daisy", "violet"],
                  ["iris", "violet"]]

treasure_rooms_1 = ["lily", "tulip", "violet", "rose"]

treasure_rooms_2 = ["lily", "jasmin", "violet"]

treasure_rooms_3 = ["violet"]

instructions_2 = [
    ["jasmin", "tulip"],
    ["lily", "tulip"],
    ["tulip", "violet"],
    ["violet", "violet"]
]

def filter_rooms(treasure_rooms, instructions):
  '''
  fulfilling condition 1
  * at least two *other* rooms have instructions pointing to this room
  '''
  match1 = {}
  match2 = {}
  for source, destination in instructions:
    if destination in match1:
      if source != destination:
        match1[destination].append(source)
    else:
      match1[destination] = []
      if source != destination:
        match1[destination].append(source)
  for key in match1.keys():
    if len(match1[key]) >= 2:
      match2[key] = match1[key]
  '''
  fulfilling condition 2
  * this room's instruction immediately points to a treasure room
  '''
  candidates = []
  for source, destination in instructions:
    for key in match2.keys():
      if key==source and destination in treasure_rooms:
        candidates.append(key)
  return candidates

'''
Complexity Analysis variables:
T: number of treasure rooms
I: number of instructions given

Time Complexity:
J<I (j being intermediate structure's keys)
O(I)*O(J) -> O(N^2)
    -  First loop populating match1 dictionary iterates once through instructions and adds by O(I)
    - Second loop that evaluates the length of values for keys and asks if they are greater than or equal to 2 (iterates once through match 2)
        * O(I)
    - Third loop is iterate through instructions and check against each key in match2, resulting in a TC that is O(I*J) where J = number of keys in match 2 which is less than that of I
    - Overall O(I+I+IJ) -> O(IJ)

Space Complexity:
M being a subset of instructions i.e unique destinations
O(M+I)
    - match1, match2, candidates
    - match1 dict stores for each destination, the sources that point to it i.e unique destinations resulting in O(M) where M is a subset of I
    - match2, subset of match1 where values have at least 2 elements, resulting in sc of O(M)
    - candidates, stores rooms that satisfy conditions resulting in sc of O(I), worst case storing all rooms
    - Overall sc is O(M+I)

'''

print(filter_rooms(treasure_rooms_1, instructions_1))
#test case
print(
  ['tulip', 'violet']== filter_rooms(treasure_rooms_1, instructions_1)
)

print(filter_rooms(treasure_rooms_2, instructions_1))
#test case
print(
  []== filter_rooms(treasure_rooms_1, instructions_1)
)

print(filter_rooms(treasure_rooms_3, instructions_2))
#test case
print(
  ['tulip']==filter_rooms(treasure_rooms_3, instructions_2)
)
