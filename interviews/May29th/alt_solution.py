# Open AI Optimization
instructions_1 = [["jasmin", "tulip"], ["lily", "tulip"], ["tulip", "tulip"],
                  ["rose", "rose"], ["violet", "rose"],
                  ["sunflower", "violet"], ["daisy", "violet"],
                  ["iris", "violet"]]

treasure_rooms_1 = ["lily", "tulip", "violet", "rose"]

treasure_rooms_2 = ["lily", "jasmin", "violet"]

treasure_rooms_3 = ["violet"]

instructions_2 = [
    ["jasmin", "tulip"],
    ["lily", "tulip"],
    ["tulip", "violet"],
    ["violet", "violet"]
]

def filter_rooms(treasure_rooms, instructions):
    # Count the number of instructions pointing to each room
    instruction_count = {}
    for source, destination in instructions:
        if destination != source:
            instruction_count[destination] = instruction_count.get(destination, 0) + 1
    # Find rooms that fulfill both conditions
    candidates = []
    for source, destination in instructions:
        if (
            destination in treasure_rooms
            and source in instruction_count
            and instruction_count[source] >= 2
        ):
            candidates.append(source)

    return candidates

'''
Complexity Analysis variables:
T: number of treasure rooms
I: number of instructions given

TC:
  - Counting # of instructions pointing to each room req iterating through instructions once: O(I)
  - Checking conditions and appending candidates requires iterating through instructions once - O(I)
  - Overall is O(I)

SC:
  - Space complexity depends on the storage req for two structures being instruction_count (type dict) and candidates (type list)
  - instruction_count dict stores count of instructions pointing to each destination
  - candidates stores rooms fulfilling conditions, can store info for all source rooms that point to treasure, thus O(N)
  - overall space complexity -> O(max(N,M))
'''

# test cases
print(['tulip', 'violet'] == filter_rooms(treasure_rooms_1, instructions_1))
print('\n')
print([] == filter_rooms(treasure_rooms_2, instructions_1))
print('\n')
print(['tulip'] == filter_rooms(treasure_rooms_3, instructions_2))
