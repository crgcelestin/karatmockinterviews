# __JUNE 4TH__

## FIND SCORES EQUAL OR ABOVE
- We're writing a new computer game, and we have a list of player scores. We'd like to figure out how many scores exceed a goal score we have in mind.

- Write a function that takes in a list of player scores and a goal, and returns the number of scores equal to or above our goal.

Examples:
``` py
scores = [1500, 1600, 1200, 700, 1900, 1300, 1400]
meets_goal(scores, 1450) => 3 (1500, 1600, and 1900 are greater)
```

## FIND FASTEST and COMMON TIMES
 - def find_fastest_times:
    - We have a list of completion times for levels for a game we're building. We'd like to figure out the fastest completion times for each level.

    - We have data for plays of each level that include the level number and time for that level. You can imagine the data looks like this.
    ```
    Level ID    Time (s)
    1            84
    2            54
    4            99
    2            56
    3            78
    1            72
    1            72
    ...(rest of data omitted)...
    ```
    - The data is not in any particular order.

    - How would you use this data to create a data structure containing the fastest completion time for each level?
         - Initial Thought Process
            def find_fastest(times)
                levels={}
                for level_id, time in times:
                    if time not in levels or levels[level_id]>time:
                        levels[level_id]=time

            What are the time and space complexities of your solution?
            O(n) - tc, sc - o(1)

- def find_common_times:
    - Next we would like to figure out the most common time for each level. For example, given the data we can see above, the most common time for level 1 is 72 seconds

    - How could we do this?
    ```py
    # MAYBE?
    level={
        '1':
            '72': [1]
            '68':[1]
            [72,1], [68,1]
    }
    ```

## EXPLORE AN ANCIENT TOMB
- While exploring an ancient tomb for treasure, you've come across a room with strange symbols on the floor. These symbols tell you which way you need to walk in order to pass through the room. However, you're not certain whether it's possible to follow these directions and reach the end of the room.

- You will be given a grid of symbols like the following:
```
(start here --->)  >   >   >   >   v
                   v   ^   >   ^   v
                   >   ^   ^   >   E
```

- You start on the square in the upper left, and need to reach the square marked "E". From each square you must follow the direction on the square. The directions are as follows:

```
 v : move down
 > : move right
 ^ : move up
 < : move left

In this case, you can reach the end square:

  [>] [>] [>] [>] [v]
   v   ^   >   ^  [v]
   >   ^   ^   >  [E]

However, the symbols may lead you off the edge.

  Right: off edge
   [>] [>] [v]
    v   ^  [>]
    <   >   E

Also, the end may not be at the bottom right.

 [>] [>] [v] [E]  v
  v   ^  [>] [^]  v
  >   ^   ^   >   >
```

- The path is guaranteed not to loop.

- Write a function that determines if it is possible to reach the end of the room from the start position in the upper left.


```py
def directed_march(floor):
    directions={
        '>'=[0,1],
        'v'=[-1,0],
        ...
    }
    pos_x=0
    pos_y=0
    #as we go through floor
        if(pos_x or pos_y <0 or pos_x or pos_y >len(floor)):
            return False
        if(floor[pos_x][pos_y]=='E'):
            return True
        pos_x+= corresponding x direction equivalent of arrow on floor at current position
        pos_y+= corresponding y direction equivalent of arrow on floor at current position
```

### ! __FEEDBACK__ !
- Practice more questions related to 2D graph iterations.
- Use print statements effectively to help with the debugging.
