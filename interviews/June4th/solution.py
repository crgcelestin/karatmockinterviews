#  FIND SCORES EQUAL OR ABOVE
def meets_goals(scores, target):
    sum=0
    for score in scores:
        if score>=target:
            sum+=1
    return sum

scores = [1500, 1600, 1200, 700, 1900, 1300, 1400]
print(meets_goals(scores, 1450))

# ---- ----- ------
# FIND FASTEST AND COMMON TIMES
record1=[
    [1,84],
    [2,54],
    [4,99],
    [2,56],
    [3,78],
    [1,72],
    [1,70],
    [4,50]
]
def find_fastest_times(level_times):
    fastest_times={}
    for level, time in level_times:
        if level not in fastest_times or fastest_times[level]>time:
            fastest_times[level]=time
    return fastest_times

'''
Complexity discussion variable:
n = number of completion times
'''

print(find_fastest_times(record1))
print('\n')
#test case
print(find_fastest_times(record1)=={1: 70, 2: 54, 4: 50, 3: 78})

def find_common_times(level_times):
    pass

print(find_common_times(record1))

# ------    ------   -------
# ! EXPLORE AN ANCIENT TOMB !
#can do this recursively, but...
def direct_march(floor):
    directions={
        '>': [0,1],
        'v': [-1,0],
        '<' : [0,-1],
        '^' : [1,0]
    }
    start_x=0
    start_y=0
    print(floor[start_x][start_y])
    while(True):
        if(start_x<0 or start_x>=len(floor) or start_y<0 or start_y>=len(floor[0])):
            return False
        if(floor[start_x][start_y]=='E'):
            return True
        start_x += directions[floor[start_x][start_y]][0]
        start_y += directions[floor[start_x][start_y]][1]

# TEST CASES
floor1 = [
    ['>', '>', '>', '>', 'v'],
    ['v', '^', '>', '^', 'v'],
    ['>', '^', '^', '>', 'E']
]

# directed_march(floor1)   => True

floor2 = [
    ['v', '>', 'v'],
    ['v', '^', '>'],
    ['<', '>', 'E']
]

# directed_march(floor2)   => False

floor3 = [
    ['v', '>', '>', 'v', '>'],
    ['>', '>', 'v', '>', '^'],
    ['^', '>', '>', '>', 'E']
]

# directed_march(floor3)   => True

floor4 = [
    ['v', '>', '>', 'v'],
    ['v', '^', '>', '>'],
    ['v', '>', '>', 'v'],
    ['v', '^', '<', 'v'],
    ['>', '>', '^', 'E']
]

# directed_march(floor4)   => True

floor5 = [
    ['>', '>', '>', '>', '>', '>', '>', '>', '>', 'v'],
    ['v', '^', '>', 'v', '<', '>', 'v', '<', '<', '<'],
    ['>', '^', 'v', '<', '^', '<', '<', '^', '>', 'E']
]

# directed_march(floor5)   => False

floor6 = [
    ['>', '>', 'v', 'E', 'v'],
    ['v', '^', '>', '^', 'v'],
    ['>', '^', '^', '>', '>']
]

# directed_march(floor6)   => True

floor7 = [
    ['>', '>', 'v', '>', 'v'],
    ['v', '^', '>', '^', 'v'],
    ['>', 'E', '^', '>', '>']
]

# directed_march(floor7)   => False

floor8 = [
    ['>', '>', 'v', '>', '^'],
    ['v', '^', '>', '^', 'v'],
    ['>', 'E', '^', '>', '>']
]

# directed_march(floor8)   => False

floor9 = [
    ['>', 'v', '>', '>', 'v', '^'],
    ['v', '<', '^', 'v', '<', 'E'],
    ['>', '>', '^', '>', '>', '^']
]

# directed_march(floor9)   => True
