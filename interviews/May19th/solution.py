'''
def find_spaces(arr):
  player_pos=find_pos(arr,'P')
  monster_pos=find_pos(arr,'M')
  x_diff=abs(player_pos[0]-monster_pos[0])
  y_diff=abs(player_pos[1]-monster_pos[1])
  print(x_diff+y_diff)
'''

def player_position(arr):
    arr_len=len(arr)
    for i in range(arr_len):
        for n in range(len(arr[i])):
            if(arr[i][n]=='P'):
                print([i,n])

def distance(arr):
    player_position=find_position(arr,'P')
    monster_position=find_position(arr,'M')
    #someList = []
    #loop through the input
    #add to the list
    return abs(player_position-monster_position)-1

def find_position(arr,target):
    arr_len=len(arr)
    for i in range(arr_len):
        if arr[i]==target:
            return i

board=['P', '-', '-', '-', '-', '-']

def alt_find_position(board):
  position = [x for x in range(len(board)) if board[x]=='P']
  return position

print(alt_find_position(board))



board1 = [
  ['-', '-', '-', '-', '-', '-'],
  ['-', '-', 'M', '-', 'M', '-'],
  ['-', '-', '-', 'P', '-', '-'],
  ['M', '-', '-', 'M', '-', '-'],
  ['-', 'M', '-', '-', '-', '-'],
]
player_position(board1) # returns 2,3

board2 = [
  ['P', '-', '-', '-', '-', '-'],
  ['-', '-', 'M', '-', 'M', '-'],
  ['-', '-', '-', '-', '-', '-'],
  ['M', '-', '-', 'M', '-', '-'],
  ['-', 'M', '-', '-', '-', '-'],
]
player_position(board2) # returns 0,0

board3 = [
  ['M', '-', 'M'],
  ['M', '-', 'P'],
]
player_position(board3) # returns 1,2
