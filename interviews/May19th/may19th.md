# MAY19TH

## __PROBLEM DESCRIPTION__

- Our user research team has discovered that two-dimensional games are all the rage; it's time to upgrade!

- New board constraints:
    - The board is a two-dimensional grid of characters
    - An empty space on the board is denoted by a dash '-'
    - The player is denoted by the letter 'P'
    - Monsters are denoted by the letter 'M'

- Write a function that takes a two-dimensional board, and returns the location of the player.

- For the location you can define whatever coordinate system works for you. A standard approach is to define the top-left corner of the board as (0,0), and give coordinates in (row,column) order.
  - Sample input and output:

```py
board1 = [
  ['-', '-', '-', '-', '-', '-'],
  ['-', '-', 'M', '-', 'M', '-'],
  ['-', '-', '-', 'P', '-', '-'],
  ['M', '-', '-', 'M', '-', '-'],
  ['-', 'M', '-', '-', '-', '-'],
]
Expected Output (in any format):
(2,3)

board2 = [
  ['P', '-', '-', '-', '-', '-'],
  ['-', '-', 'M', '-', 'M', '-'],
  ['-', '-', '-', '-', '-', '-'],
  ['M', '-', '-', 'M', '-', '-'],
  ['-', 'M', '-', '-', '-', '-'],
]
Expected Output (in any format):
(0,0)

board3 = [
  ['M', '-', 'M'],
  ['M', '-', 'P'],
]
Expected Output (in any format):
(1,2)

Complexity Analysis variables:
n = number of rows
m = number of columns

'''
for(n in board) #rows
 for(m in board[n]) #columns
   (if board[n][m]==='P')
     return (n,m) -> 'P'
'''
```

## General Suggestions/Improvements
- Work on improving python as that is a language that is utilized in majority of deployments
  - Faster, intuitive implementation of 2d board solution


- 3 Pillars
  - Know language details
  - Know Data Structures
  - Know Algos
    - Express ideas from problems into code
  - Focus on 1 language in DS and algos context
    - On top of LC, being an expert with language's particular library
    - Increased exp = increased knowledge depth
  - If learning 2 languages, will be tested on both

__Time Complexity__
- Always worst case when discussing TC i.e Big O
  - In interviews ask about specifics of what is to be taken into consideration (Big O vs Big Theta)
  - If you can build another solution, shows a good signal
  - O(n) signifies that the variable is growing in a linear fashion with input

__Extraneous__
- Space complexity changes with each language
  - JavaScript is a Statically Typed Language as types are defined as code is compiled
  - Python has constant values stored via reference
