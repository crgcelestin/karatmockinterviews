function playerPosition(arr) {
    for (let i = 0; i < arr.length; i++) {
        for (let j = 0; j < arr[i].length; j++) {
            if (arr[i][j] === 'P') {
                return [i, j]
            }
        }
    }
}

function distance(arr) {
    const player_position = findPosition(arr, 'P');
    const monster_position = findPosition(arr, 'M');
    return Math.abs(player_position - monster_position);
}

function findPosition(arr, target) {
    const length = arr.length;
    for (let i = 0; i < length; i++) {
        if (arr[i] === target) {
            return i;
        }
    }
}
