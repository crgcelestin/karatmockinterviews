/*
All Test Cases:
mismatches(records1) => ["Steve", "Curtis", "Paul", "Joe"], ["Martha", "Pauline", "Curtis", "Joe"]
mismatches(records2) => [], []
mismatches(records3) => ["Paul"], ["Paul"]
mismatches(records4) => ["Raj", "Paul"], ["Paul"]

n: length of the badge records array
*/

"use strict";

const records1 = [
    ["Paul", "enter"],
    ["Pauline", "exit"],
    ["Paul", "enter"],
    ["Paul", "exit"],
    ["Martha", "exit"],
    ["Joe", "enter"],
    ["Martha", "enter"],
    ["Steve", "enter"],
    ["Martha", "exit"],
    ["Jennifer", "enter"],
    ["Joe", "enter"],
    ["Curtis", "exit"],
    ["Curtis", "enter"],
    ["Joe", "exit"],
    ["Martha", "enter"],
    ["Martha", "exit"],
    ["Jennifer", "exit"],
    ["Joe", "enter"],
    ["Joe", "enter"],
    ["Martha", "exit"],
    ["Joe", "exit"],
    ["Joe", "exit"]
];

const records2 = [
    ["Paul", "enter"],
    ["Paul", "exit"]
];

const records3 = [
    ["Paul", "enter"],
    ["Paul", "enter"],
    ["Paul", "exit"],
    ["Paul", "exit"]
];

const records4 = [
    ["Raj", "enter"],
    ["Paul", "enter"],
    ["Paul", "exit"],
    ["Paul", "exit"],
    ["Paul", "enter"],
    ["Raj", "enter"]
];

/*
  records 2 output=[]
  enter->exit

  for i=0<records
  j=i+1,j<records
  i 0 raj enter
  j 0 1 2 3 4 5
  raj enter

*/

// best solution so far
function badge_enter_exit(records) {
    const enters = new Set();
    const exits = new Set();
    const missing_exits = new Set();
    const missing_enters = new Set();

    for (let [employee, action] of records) {
        if (action === 'enter') {
            enters.add(employee);
            if (exits.has(employee)) {
                exits.delete(employee);
            } else {
                missing_enters.add(employee);
            }
        } else {
            exits.add(employee);
            if (enters.has(employee)) {
                enters.delete(employee);
            } else {
                missing_exits.add(employee);
            }
        }
    }

    const result1 = Array.from(missing_exits);
    const result2 = Array.from(missing_enters);

    return [result1, result2];
}




function enter_exit(record_names) {
    const unique = new Set()
    for (let i = 0; i < record_names.length; i++) {
        if (record_names[i][1] === 'enter') {
            unique.add(record_names[i][0])
        }
        if (record_names[i][1] === 'exit') {
            unique.delete(record_names[i][0])
        }
    }
    return unique
}

function unique_names(records) {
    const names = new Set()
    for (let i = 0; i < records.length; i++) {
        names.add(records[i][0])
    }
    return names
}

function name_order(record, name) {
    for (let i = 0; i < record.length; i++) {
        if (record[i] === name) {
            return i
        }
    }
    return
}
