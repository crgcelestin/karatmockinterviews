records1 = [
    ["Paul", "enter"],
    ["Pauline", "exit"],
    ["Paul", "enter"],
    ["Paul", "exit"],
    ["Martha", "exit"],
    ["Joe", "enter"],
    ["Martha", "enter"],
    ["Steve", "enter"],
    ["Martha", "exit"],
    ["Jennifer", "enter"],
    ["Joe", "enter"],
    ["Curtis", "exit"],
    ["Curtis", "enter"],
    ["Joe", "exit"],
    ["Martha", "enter"],
    ["Martha", "exit"],
    ["Jennifer", "exit"],
    ["Joe", "enter"],
    ["Joe", "enter"],
    ["Martha", "exit"],
    ["Joe", "exit"],
    ["Joe", "exit"]
];

records2 = [
    ["Paul", "enter"],
    ["Paul", "exit"]
];

records3 = [
    ["Paul", "enter"],
    ["Paul", "enter"],
    ["Paul", "exit"],
    ["Paul", "exit"]
];

records4 = [
    ["Raj", "enter"],
    ["Paul", "enter"],
    ["Paul", "exit"],
    ["Paul", "exit"],
    ["Paul", "enter"],
    ["Raj", "enter"]
];

def badge_enter_exit(records):
    pass

def enter_exit(record_names):
    pass

def unique_names(records):
    pass
