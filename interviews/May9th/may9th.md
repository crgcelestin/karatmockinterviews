# MAY9TH

## __PROBLEM DESCRIPTION__

- Imagine we have an image. We'll represent this image as a simple 2D array where every pixel is a 1 or a 0. The image you get is known to have a single rectangle of 0s on a background of 1s.

- Write a function that takes in the image and returns one of the following representations of the rectangle of 0's: top-left coordinate and bottom-right coordinate OR top-left coordinate, width, and height.

```js
image1 = [
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 0, 0, 0, 1],
  [1, 1, 1, 0, 0, 0, 1],
  [1, 1, 1, 1, 1, 1, 1],
]
```

- Sample output variations (only one is necessary):

```js
findRectangle(image1) =>
  row: 2, column: 3, width: 3, height: 2
  2,3 3,5 -- row,column of the top-left and bottom-right corners


Other test cases:

image2 = [
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 0],
]

findRectangle(image2) =>
  row: 4, column: 6, width: 1, height: 1
  4,6 4,6

image3 = [
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 0, 0],
  [1, 1, 1, 1, 1, 0, 0],
]

findRectangle(image3) =>
  row: 3, column: 5, width: 2, height: 2
  3,5 4,6

image4 = [
  [0, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1],
]

findRectangle(image4) =>
  row: 0, column: 0, width: 1, height: 1
  0,0 0,0

image5 = [
  [0],
]

findRectangle(image5) =>
  row: 0, column: 0, width: 1, height: 1
  0,0 0,0

All Test Cases:
(top-left, width, height) or (top-left, bottom-right)
findRectangle(image1) => 2,3 3,2  or  2,3 3,5
findRectangle(image2) => 4,6 1,1  or  4,6 4,6
findRectangle(image3) => 3,5 2,2  or  3,5 4,6
findRectangle(image4) => 0,0 1,1  or  0,0 0,0
findRectangle(image5) => 0,0 1,1  or  0,0 0,0

n: number of rows in the input image
m: number of columns in the input image
```

### Idea for Implementation
1. two for loops
    - one that goes through rows (i)
    one that goes through columns (j)
  3. conditional asking matrix[i][j]===0
  4. push each instance of i and j where this case is matched
  5. return first and last of indices
    - indices[0], indices[indices.length-1]


## __FEEDBACK__
### Time Complexity, Space Complexity
- Advice
  - Look at worst complexity
  - How does sc, tc change with changes made to input
  - Navigating 2d grids, test implementation incrementally

- __Find Rectangle__
   - O(n*m) for time complexity as well as for space due width and height being different
   - Only returning 1 array so its O(1) constant for space

- __Get Width__
  - Space complexity is O(1) and Time complexity is O(n)
