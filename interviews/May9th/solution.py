image1 = [
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 0, 0, 0, 1],
    [1, 1, 1, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1],
];

def findDimensions(matrix):
    indices=[]
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if matrix[i][j]==0:
                indices.append([i,j])
    row=indices[0][0]
    column=indices[0][1]
    height=abs(row-indices[len(indices)-1][0])+1
    width=abs(column-indices[len(indices)-1][1])+1
    return(f'row: {row}, column: {column}, width: {width}, height: {height}')

def findFirst0(matrix):
    indices=[]
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if matrix[i][j]==0:
                indices.append([i,j])
                return indices

print(findFirst0(image1))
print(findDimensions(image1))

def getWidth(arr):
    x=0
    for i in range(len(arr)):
        if arr[i]==0:
            x+=1
    return x


def getZeroIndex(arr):
    zeroIndex=[x for x in range(len(arr)) if arr[x]==0]
    return zeroIndex

arr=[0, 1, 1, 1, 1, 1, 1]
print(getZeroIndex(arr))
