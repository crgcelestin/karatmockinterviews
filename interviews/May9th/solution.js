"use strict";

//verbose method
function findRectangle3(matrix) {
    let indices = []
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            if (matrix[i][j] === 0) {
                indices.push([i, j])
                break;
            }
        }
    }
    for (let i = matrix.length - 1; i <= 0; i--) {
        for (let j = matrix[i].length - 1; j <= 0; j--) {
            if (matrix[i][j] === 0) {
                indices.push([i, j])
            }
        }
    }
}

//preferred method
function findRectangle(matrix) {
    let indices = []
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            if (matrix[i][j] === 0) {
                indices.push([i, j])
            }
        }
    }
    return [indices[0], indices[indices.length - 1]];
}

//finds first instance of 0 occuring in 2d matrix
function findRectangle2(matrix) {
    let indices = []
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            if (matrix[i][j] === 0) {
                indices.push([i, j])
                return indices
            }
        }
    }

}


function getWidth(arr) {
    const lookup = {}
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === 0 && lookup[0]) {
            lookup[0] += 1
        } else if (arr[i] === 0 && !lookup[0]) {
            lookup[0] = 1
        } else {
            continue
        }
    }
    return lookup
}

//preferred method
function getWidth(arr) {
    let x = 0;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === 0) {
            x++;
        }
        return x;
    }
}

function firstZero(arr) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === 0) {
            return i;
        }
    }
    return null;
}

function alt_firstZero(arr) {
    const index = arr.map((e, i) => e === 2 ? i : undefined).filter(x => x !== undefined)
    return index
}
arr1 = [0, 1, 1, 1, 1, 1, 1]
console.log(alt_firstZero(arr1))

const image1 = [
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 0, 0, 0, 1],
    [1, 1, 1, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1],
];

const image2 = [
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 0],
];

const image3 = [
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 0, 0],
    [1, 1, 1, 1, 1, 0, 0],
];

const image4 = [
    [0, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
];

const image5 = [
    [0],
];
console.log(findRectangle(image1))
console.log(findRectangle(image2))
console.log(findRectangle(image3))
console.log(findRectangle(image4))
console.log(findRectangle(image5))
