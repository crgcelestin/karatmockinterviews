//problem 1
function AnalyzeListeningTime(durations, minutes) {
    let songs = 0;
    for (let duration of durations) {
        if (minutes > duration || minutes === duration) {
            minutes -= duration
            songs += 1
        }
        else {
            break
        }
    }
    return songs
}

const durations = [1, 2, 3]
const minutes = 3

console.log(AnalyzeListeningTime(durations, minutes))

//problem 2
const products = [
    ["Video Card", 1000, "newegg.com"],
    ["T-Shirt", 10, "walmart.com"],
    ["T-Shirt", 15, "handm.com"],
    ["24 Inch Monitor", 250, "bestbuy.com"],
    ["20 Inch Monitor", 200, "bestbuy.com"],
    ["Video Card", 1200, "bestbuy.com"],
    ["Video Card", 1100, "google.com"],
    ["24 Inch Monitor", 240, "google.com"],
    ["20 Inch Monitor", 210, "google.com"],
]

function MinSiteProduct(products, tracker = {}) {
    for (let [product, price, website] in products) {
        if (!Object.keys(tracker).includes(product) || (price < tracker[product][0])) {
            tracker[product] = [price, website];
        }
    }
    return tracker;
}

console.log(MinSiteProduct(products))

//problem 3
const room = [
    ['S', '-', '-', '-', 'D'],
    ['-', 'D', 'D', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['D', 'D', '-', '-', '-'],
    ['-', 'D', '-', 'A', '-'],
    ['-', 'D', '-', '-', '-']
]

function detected(room, directions) {
    const counter = {
        'S': [1, 0],
        'N': [-1, 0],
        'E': [0, 1],
        'W': [0, -1]
    }
    const S_X = 0
    const S_Y = 0
    const helper = (x, y, remaining_dir) => {
        if (
            x < 0 || y < 0 ||
            x >= room.length || y >= room.length ||
            room[x][y] == "D"
           ){
            return false
        }
        if (room[x][y] === 'A') {
            return true
        }
        if (!remaining_dir) {
            return False
        }
        let next_dir = remaining_dir[0]
        let deltaX = counter[next_dir][0]
        let deltaY = counter[next_dir][1]
        let new_X = x + deltaX
        let new_Y = y + deltaY
        return helper(new_X, new_Y, remaining_dir.slice(1))
    }
    return helper(S_X, S_Y, directions)
}

console.log(detected(room, "SSEEESS")) //true
console.log(detected(room, "ESSSSEE")) //false
console.log(detected(room, "EEENSSSSS")) //false
console.log(detected(room, "EEESESSSSWNNNN")) //true
