#First Problem
def songs(arr, minutes):
    songs=0
    for i in range(len(arr)):
        if(minutes>arr[i] or minutes==arr[i]):
            minutes-=arr[i]
            songs+=1
        else:
            break
    return songs

'''
playlist = [5, 8, 4, 1, 10]
print(songs(playlist, 10)) #returns 1
print(songs(playlist, 20)) #returns 4
print(songs(playlist, 13)) #returns 2
print(songs(playlist, 3)) #returns 0
print(songs(playlist, 100)) #returns 5
'''

#Second Problem
"""
APPROACH
(0)
(a) give data structure is an array with subarrays that are indexed with 0 being the product, 1 being the price of said product, 2 being the website
(b) Initialize a record that tracks instances of a particular product as a key, as a value for a specific key include instances where said product is sold
    example:
    products={
        'video card' = [[1000, 'newegg.com'], [1200, 'bestbuy.com']]
    }
(1) iterate through each product in products
(2) while iterating,
    find min in instances of product with prices
(3) return min dict with min instances of all products

or!

(b) Initialize record that only tracks min of any selling of a product or only instance of product being sold if there are not multiple
"""

'''
initial thought process
I knew I had to (MAYBE) get it to this eventual structure in order to get minimums at each site

products= {
  'Video Card': [[1000, 'newegg.com'], [1200, 'bestbuy.com'], [1100, 'google.com']]
}
price=min([x for x in products['video card']])
print(price)

'''
products = [
    ["Video Card", 1000, "newegg.com"],
    ["T-Shirt", 10, "walmart.com"],
    ["T-Shirt", 15, "handm.com"],
    ["24 Inch Monitor", 250, "bestbuy.com"],
    ["20 Inch Monitor", 200, "bestbuy.com"],
    ["Video Card", 1200, "bestbuy.com"],
    ["Video Card", 1100, "google.com"],
    ["24 Inch Monitor", 240, "google.com"],
    ["20 Inch Monitor", 210, "google.com"],
]

# my solution
def sub_dissect(products):
  count={}
  mins={}
  for product in products:
    count[product[0]]=[]
  for product in products:
    count[product[0]].append([product[1],product[2]])
  for i in count.keys():
    mins[i]=min([x for x in count[i]])
  return mins

# chat
def min_site_product(products):
   tracker={}
   #utilizing tuple unpacking which can be used on any list/tuple
   #Eliminates unnecessary intermediate lists
   for product, price, website in products:
      if product not in tracker or price < tracker[product][0]:
         tracker[product]=[price, website]
   return tracker

print(sub_dissect(products))
print(min_site_product(products))

#Third Problem
"""
All Test Cases:
detected(room, "SSEEESS")        => True (got the artwork)
detected(room, "ESSSSEE")        => False (Got detected)
detected(room, "EEENSSSSS")      => False (Hit the wall)
detected(room, "EEESEESSSWW")    => False (Hit the wall)
detected(room, "EEESESSSSWNNNN") => True (got the artwork)
detected(room, "EEESSS")         => False (did not reach the artwork)
detected(room, "NEEESEESSSWW")   => False (Hit the wall)
detected(room, "SWWSSS")         => False (Hit the wall)
detected(room, "SSEEESSWW")      => True (got the artwork)

* INITIAL THOUGHT PROCESS*
2-d arrays,
for i in range(len(room)):
    for j in range(len(room[i])):
        if direction -> i out of bonds
            break
        if direction -> i in bounds
            s = + [1,0]
            n = - [1,0]
            e = + [0,1]
            w = - [0,1]
                2 (s)
                3 (e)
                2 (s)
                if ('D'):
                    return False
                    break
                if('A'):
                    return True
                    break
        return False
"""
#recursive solution, recursive seemed the best way to go in terms of performance and implementation
def detected(room, directions):
    #establish dict to convert direction into a move
    counter = {
        'S': [1, 0],
        'N': [-1, 0],
        'E': [0, 1],
        'W': [0, -1]
    }
    #establish start, player always start at origin
    S_X, S_Y = 0, 0

    #helper function to move across board and eval if outside bounds, D is encountered, or A is encountered
    #takes in position and given directions
    def helper(x, y, remaining_directions):
        '''
        Base cases
            (1) bad base cases, if out of bounds or encountered security system
            (2) good base case, found artwork
            (3) if directions end and neither base case is reached
        '''
        if x < 0 or y < 0 or x >= len(room) or y >= len(room[0]) or room[x][y] == 'D':
            return False
        if room[x][y] == 'A':
            return True
        if not remaining_directions:
            return False

        #next direction is the first of the string with possibilities within 'SNEW'
        next_dir = remaining_directions[0]
        # convert that direction into its equivalency in terms of numerical move
        deltaX, deltaY = counter[next_dir]
        #add that converted direction to the current position of the player on the board
        new_X, new_Y = x + deltaX, y + deltaY
        #recursive invocation with current position, splice directions with previous direction excluded
        return helper(new_X, new_Y, remaining_directions[1:])

    #invoke helper function
    return helper(S_X, S_Y, directions)

room = [
    ['S', '-', '-', '-', 'D'],
    ['-', 'D', 'D', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['D', 'D', '-', '-', '-'],
    ['-', 'D', '-', 'A', '-'],
    ['-', 'D', '-', '-', '-']
]

print(detected(room, "SSEEESS"))
# # => True (got the artwork)
print(detected(room, "ESSSSEE"))
# => False (Got detected))
print(detected(room, "EEENSSSSS"))
# # => False (Hit the wall)
print(detected(room, "EEESEESSSWW"))
#       # => False (Hit the wall)
print(detected(room, "EEESESSSSWNNNN"))
#       # => True (got the artwork)
print(detected(room, "EEESSS"))
#       # => False (did not reach the artwork)
print(detected(room, "NEEESEESSSWW"))
#       # => False (Hit the wall)
print(detected(room, "SWWSSS"))
#       # => False (Hit the wall)
print(detected(room, "SSEEESSWW"))
#       # => True (got the artwork)
