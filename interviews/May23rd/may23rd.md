# MAY19TH

## __PROBLEM DESCRIPTIONS__
- Problem 1
    - Given an array of song durations as integers and an alloted time given in minutes as an integer return the number of songs possible to be listened to in a given period of minutes
    - Example Test Cases
      ```py
        playlist = [5, 8, 4, 1, 10]
        print(songs(playlist, 10)) #returns 1
        print(songs(playlist, 20)) #returns 4
        print(songs(playlist, 13)) #returns 2
        print(songs(playlist, 3)) #returns 0
        print(songs(playlist, 100)) #returns 5
      ```
- Problem 2
    - You are searching online for a series of products and you've found the same items on various websites for different prices.
    - Given an array of data, with each entry containing the product product, price and website, how could we create a data structure containing the cheapest price for each item and which website this price was on.
    - Expected Results given Input
      ```py
      Example:
        products = [
            ["Video Card", 1000, "newegg.com"],
            ["T-Shirt", 10, "walmart.com"],
            ["T-Shirt", 15, "handm.com"],
            ["24 Inch Monitor", 250, "bestbuy.com"],
            ["20 Inch Monitor", 200, "bestbuy.com"],
            ["Video Card", 1200, "bestbuy.com"],
            ["Video Card", 1100, "google.com"],
            ["24 Inch Monitor", 240, "google.com"],
            ["20 Inch Monitor", 210, "google.com"],
        ]

        Expected results:
        Video Card: 1000, newegg.com
        T-Shirt: 10, walmart.com
        24 Inch Monitor: 240, google.com
        20 Inch Monitor: 200, bestbuy.com
      ```
- Problem 3
    -As a spy, you've been tasked with recovering some stolen artwork. You've entered the room containing the artwork, and have detected some security systems in place.
    - The room is represented as a matrix, where the spy "S" is in the top left (0,0), open locations are "-", spaces with the security system are "D" and the artwork is "A".
    - Example of Room
    ```py
        room = [
            ['S', '-', '-', '-', 'D'],
            ['-', 'D', 'D', '-', '-'],
            ['-', '-', '-', '-', '-'],
            ['D', 'D', '-', '-', '-'],
            ['-', 'D', '-', 'A', '-'],
            ['-', 'D', '-', '-', '-'],
        ]
    ```
    - You've been provided with a series of instructions. These are provided as a string of directions to move, using the characters N (north/up), S (south/down), E (east/right), W (west/left).
    - Example: "NNSSWW" = North, North, South, South, West, West.
    - Given a room and a list of instructions. Return whether you reach the artwork without getting detected. You are detected if you hit the security system or the walls of the room before reaching the artwork.



## General Suggestions/Improvements
- Attempt incremental testing and debugging of code using print statements in order to recognize when to use breaks, continues
- Work through questions involving 2D arrays as underlying data structures
