# APRIL18

- An activity at our university requires students to work in pairs.
- Given a list of student ID number, course name pairs, write a function that returns a collection of all possible pairs of student ID numbers.

```javascript
// Sample input and output:

enrollments1 = [
  ["58", "Linear Algebra"],
  ["94", "Art History"],
  ["94", "Operating Systems"],
  ["17", "Software Design"],
  ["58", "Mechanics"],
  ["58", "Economics"],
  ["17", "Linear Algebra"],
  ["17", "Political Science"],
  ["94", "Economics"],
  ["25", "Economics"],
  ["58", "Software Design"],
]

Expected output: [
  "58,17",
  "58,94",
  "58,25",
  "94,25",
  "17,94",
  "17,25"
]

enrollments2 = [
  ["0", "Advanced Mechanics"],
  ["0", "Art History"],
  ["1", "Course 1"],
  ["1", "Course 2"],
  ["0", "Computer Architecture"],
]

Expected output: [
  "0,1"
]

/*
Complexity analysis variables:

n: number of student course pairs in the input
s: number of students
*/
```

## __FEEDBACK__
- Don't delete code for previous problem
- Check if one can reuse previous solutions in next problems
- Keep a consistent coding style related to variable/function naming, spaces, indentation, semicolons
- First describe approach -> then code (avoid typing w/o having full approach in mind)
- Hash maps have keys, values, if not using values -> set = better choice
