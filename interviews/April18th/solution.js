const enrollments1 = [
    ["58", "Linear Algebra"],
    ["94", "Art History"],
    ["94", "Operating Systems"],
    ["17", "Software Design"],
    ["58", "Mechanics"],
    ["58", "Economics"],
    ["17", "Linear Algebra"],
    ["17", "Political Science"],
    ["94", "Economics"],
    ["25", "Economics"],
    ["58", "Software Design"]
];

function returnPairs(enrollments) {
    const nums = []
    const ids = Student_Numbers(enrollments);
    const results = []
    for (let ele of ids) {
        nums.push(ele)
    }
    for (let i = 0; i < nums.length; i++) {
        for (let j = i + 1; j < nums.length; j++) {
            results.push([nums[i], nums[j]])
        }
    }
    return results
}
console.log(returnPairs(enrollments1))

const enrollments2 = [
    ["0", "Advanced Mechanics"],
    ["0", "Art History"],
    ["1", "Course 1"],
    ["1", "Course 2"],
    ["0", "Computer Architecture"]
];


function Student_Numbers(enrollments) {
    let lookup = new Set()
    for (let i = 0; i < enrollments.length; i++) {
        if (!lookup[enrollments[i][0]]) {
            lookup.add(enrollments[i][0])
        } else {
            continue
        }
    }
    return lookup
}


console.log(Student_Numbers(enrollments1))

function course_position(schedule, findClass) {
    for (let i = 0; i < schedule.length; i++) {
        if (schedule[i] === findClass) {
            return i;
        }
    }
    return null;
}

  // // console.log(course_position(schedule1,'Political Science'))
