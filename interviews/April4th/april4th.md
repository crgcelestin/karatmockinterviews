# APRIL 4TH

- Find all of the distinct individuals given only the list of parent-child relationships

- Write a function that takes the parent-child relationships as input, and returns a list of every individual as a unique integer.

```javascript
// Sample input and output:
    1   2    4
     \ /   / | \
      3   5  8  9
       \ / \     \
        6   7    11

parent_child_pairs = [
    (1, 3), (2, 3), (3, 6), (5, 6),
    (5, 7), (4, 5), (4, 8), (4, 9), (9, 11)
]

// Returns
[1, 2, 3, 4, 5, 6, 7, 8, 9, 11]

/*
Complexity Analysis variables:
n: the number of pairs in the input
*/
```

## FEEDBACK
- Attempt to simplify the approach, use inputs as given, and output manipulation
