def return_unique(pairs):
    lookup=set()
    for pair in pairs:
        for num in pair:
            if num not in lookup:
                lookup.add(num)
            else:
                continue
    return lookup

parent_child_pairs = [
    (1, 3), (2, 3), (3, 6), (5, 6),
    (5, 7), (4, 5), (4, 8), (4, 9), (9, 11)
]

print(return_unique(parent_child_pairs))

def is_parent_of(pairs, num1, num2):
    arr=(num1, num2)
    length=len(pairs)
    for pair in range(length):
        if arr==pairs[pair]:
          return True
        if arr!=pairs[pair] and pair<length:
          continue
    return False

print(is_parent_of(parent_child_pairs, 1,3,))
