"use strict";

function return_unique(pairs) {
    /*
      pairs as input
      iterate through pairs
        for pairs of parentChildPairs
        expected order (parent is first, child follows)
        a,b (a=parent, b=child)
        1=parent, 3=child, return true 3 directly 1
        1=parent, 9=child, return false
    */
    var lookup = {}
    var output = []
    const numberCompare = (a, b) => {
        return a - b;
    }
    for (let pair of pairs) {
        if (!lookup[pair[0]] && !lookup[pair[1]]) {
            lookup[pair[0]] = pair[1], lookup[pair[1]] = 1;
            output.push(pair[0], pair[1])
        } else if (!lookup[pair[0]] && lookup[pair[1]]) {
            lookup[pair[0]] = 1
            output.push(pair[0])
        } else if (lookup[pair[0]] && !lookup[pair[1]]) {
            lookup[pair[1]] = 1
            output.push(pair[1])
        }
    }
    return output.sort(numberCompare)
}

//TC: O(N), as input so does number of steps
//sc: O(1)
const parentChildPairs = [
    [1, 3], [2, 3], [3, 6], [5, 6],
    [5, 7], [4, 5], [4, 8], [4, 9], [9, 11]
];
console.log(return_unique(parentChildPairs))

//doesn't work as js checks equivalencies for identity and not value, they don't ref the same ID
function is_parent_of(arr, num1, num2) {
    const parent_child = [num1, num2]
    for (let pair of arr) {
        if (parent_child === pair) {
            return true;
        }
    }
    return false;
}
// console.log(is_parent_of(parentChildPairs, 1, 3))
// console.log(is_parent_of(parentChildPairs, 5,6))
// is_parent_of(parent_child_pairs, 1, 3) --> True
// is_parent_of(parent_child_pairs, 3, 1) --> False # parent is always first in pair
// is_parent_of(parent_child_pairs, 1, 9) --> False
// is_parent_of(parent_child_pairs, 1, 6) --> False # not transitive, not grandparents

console.log(is_parent_of(parentChildPairs, 1, 3))
