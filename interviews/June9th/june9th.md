# JUNE 9TH

[JUNE 9TH SOLUTION](solution.py)
## __1ST PROBLEM [ ARRAY TRAVERSAL ]__

We're prototyping some puzzles for a game we're working on.

In this puzzle, there are lasers that shoot out beams of light to the left and right, and there is a wall that stops the beams of light. We want to find out if any of the beams of light from the lasers hit other lasers.

For example, consider the following layout.
```
...L..W.L
```

The L indicates a laser that will shoot a beam of light, and the W indicates a wall. Here, the beams of light from the lasers do not hit each other since they are separated by a wall. However:
```
...L.L..W.L
```

Here, the two lasers on the left hit each other, since they are not separated by a wall.

There may be any number of lasers, but there will always be exactly one wall.

Write a function that determines if any of the lasers hit each other. Return True if they do, and False if they do not.

---

## __2ND PROBLEM [ 2D ARRAYS ]__
In this next puzzle, there are lasers on a rectangular grid. The grid might look something like this:
```
.........
.....W...
.........
...WWW...
..W......
..W......
```

In this grid, a "." represents an empty space in which a laser may be placed, and a "W" represents a wall.

There are also several types of lasers, as illustrated below. (| and - symbols are for illustration only)
```
 Right    Left      Up      Down     None
......   ......   ......   ......   ......
......   ......   ..W...   ..v...   ......
.>--W.   ......   ..|...   ..|...   ......
......   ---<..   ..|...   ..|...   ..N...
......   ......   ..^...   ..|...   ......
......   ......   ......   ..|...   ......
```
- '>, <, ^, v' beams shoot beams up light right, left, up, and down respectively. N lasers fire no beams, but may get hit by other beams.

Beams of light do not go through walls or other lasers.

You will be given a grid with lasers on it. Return the number of lasers that are hit by beams of light from other lasers.

## __FEEDBACK__

### Interview

- Solve a high quantity of low-difficulty questions on a regular basis

- Practice using print statements to debug.
    - Ask yourself the question, "What new information would help me track down the error?" This could be an index, a print before a return statement, a print of a dictionary you're building, etc.

- Familiarize yourself with useful Python standard library imports such as collections, math, and itertools

- Get to point of fluid articulation i.e. translating English to Code

- Use Code Wars, with coding platforms view solutions and use them in pattern recognition

### Notes

- #### Pre Interview Quiz
    - NaN = Not a Number (error in integer calculations ex: int + string)

    - Jagged Array (classification of an array )
        ```py
        # 2D Array that is uncommon and is an unconventional matrix that contains subarrays with varying number of elements
            [
                [a,b,c],
                [a,b],
                [a]
            ]
        ```

    - Worst case Time Complexity for Binary Search Tree is O(n) [ Big O ]
        - This is due to a potential extremely unbalanced tree (n being the height of the skewed tree)
        - BST is either left-skewed or right-skewed turning BST into linear search over all items, with target node of search being at end of skewed tree
            - Adding nodes to a tree that have values that continue to be lesser or greater than the prior node (parent node) added
            - ![Skewed Tree](img/SkewedTree.jpg)

    - Unions
        - A U B -> Combines two sets i.e concept of membership
        ```py
        A = {a, e, i, o, u}
        B = {a, b, c, d, e, f}
        A∪B = {a, b, c, d, e, f, i, o, u} # Union Product
        '''
        len(A) [5] + len(B) [6] - Intersection of A and B [common elements between sets] = 9
        '''
        A∩B = {a, e} # Intersection Product
        ```

    - String Interning
        - method of storing only one copy of each distinct string value in memory, which must be immutable

    - Space Complexity of Adjacency Matrix
        - O(V^2) [ For every node it visits every other node in the worst case complexity ] with v being every vertex in a graph structure
