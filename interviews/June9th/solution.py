"""
All Test Cases:
lasers(puzzle1) => False
lasers(puzzle2) => True
lasers(puzzle3) => True
lasers(puzzle4) => True
lasers(puzzle5) => False
lasers(puzzle6) => False
lasers(puzzle7) => True
lasers(puzzle8) => True

Complexity Analysis variable:
n = length of the puzzle

def lasers (arr)
 found_laser=False
 find first occurence of L (iterate) []
    if find laser (if x == 'L'):
        found_laser=True
    if find W
        found_laser=False
    if found_laser and x == 'L':
        return True
  return False
"""
def lasers(arr):
    found_laser=False
    for i,ele in enumerate(arr):
        if found_laser and ele == 'L':
            return True
        elif ele == 'W':
            found_laser=False
        elif ele == 'L':
            found_laser=True
    return False

puzzle1 = ['.', '.', '.', 'L', '.', '.', 'W', '.', 'L']
puzzle2 = ['.', '.', '.', 'L', '.', 'L', '.', '.', 'W', '.', 'L']
puzzle3 = ['L', 'L', 'W']
puzzle4 = ['W', 'L', 'L']
puzzle5 = ['.', '.', 'L', '.', '.', 'W', '.', '.', '.', '.']
puzzle6 = ['W']
puzzle7 = ['L', '.', 'L', '.', 'L', '.', 'W', '.', 'L']
puzzle8 = ['L', '.', 'W', '.', 'L', '.', 'L']

# ['W', 'L', '.', '.', '.']

print(lasers(puzzle1))
print(lasers(puzzle2))
print(lasers(puzzle3))
print(lasers(puzzle4))
print(lasers(puzzle5))
print(lasers(puzzle6))
print(lasers(puzzle7))
print(lasers(puzzle8))

"""
All Test Cases:
laser_grid(grid1)  => 0
laser_grid(grid2)  => 1
laser_grid(grid3)  => 1
laser_grid(grid4)  => 0
laser_grid(grid5)  => 1
laser_grid(grid6)  => 1
laser_grid(grid7)  => 0
laser_grid(grid8)  => 1
laser_grid(grid9)  => 1
laser_grid(grid10) => 0
laser_grid(grid11) => 1
laser_grid(grid12) => 1
laser_grid(grid13) => 3
laser_grid(grid14) => 4
laser_grid(grid15) => 5
laser_grid(grid16) => 1
laser_grid(grid17) => 4

Complexity Analysis variables:
r = number of rows in the grid
c = number of columns in the grid

Hint: You may not be able to implement all of this in the time for this interview. You will receive partial credit for each laser you implement the logic for.
"""
# Left lasers
grid1 = [
    ['.', '.', '.', '.'],
    ['.', '.', '<', '.'],
    ['.', '<', '.', '.'],
    ['.', '.', '.', '.']
] # 0

grid2 = [
    ['.', '.', '.', '.'],
    ['.', 'N', '<', '.'],
    ['.', '.', '.', '.'],
    ['.', '.', '.', '.'],
    ['.', '.', '.', '.']
] # 1

grid3 = [
    ['<', 'W', '.', '.', '<'],
    ['.', '.', '.', '.', '.'],
    ['.', '.', '.', '.', '.'],
    ['<', '.', '.', '.', '<']
] # 1

# Right lasers
grid4 = [
    ['.', '.', '.', '.'],
    ['.', '.', '>', '.'],
    ['.', '>', '.', '.'],
    ['.', '.', '.', '.']
] # 0

grid5 = [
    ['.', '.', '.', '.', '.'],
    ['.', '>', 'N', '.', '.'],
    ['.', '.', '.', '.', '.'],
    ['.', '.', '.', '.', '.']
] # 1

grid6 = [
    ['>', '.', 'W', '>'],
    ['.', '.', '.', '.'],
    ['.', '.', '.', '.'],
    ['.', '.', '.', '.'],
    ['>', '.', '.', '>']
] # 1

# Down lasers
grid7 = [
    ['.', '.', '.', '.'],
    ['.', '.', 'v', '.'],
    ['.', 'v', '.', '.'],
    ['.', '.', '.', '.']
] # 0

grid8 = [
    ['.', '.', '.', '.', '.'],
    ['.', 'v', '.', '.', '.'],
    ['.', 'N', '.', '.', '.'],
    ['.', '.', '.', '.', '.']
] # 1

grid9 = [
    ['v', '.', '.', 'v'],
    ['.', '.', '.', '.'],
    ['.', '.', '.', '.'],
    ['W', '.', '.', '.'],
    ['v', '.', '.', 'v']
] # 1

# Up lasers
grid10 = [
    ['.', '.', '.', '.'],
    ['.', '.', '^', '.'],
    ['.', '^', '.', '.'],
    ['.', '.', '.', '.']
] # 0

grid11 = [
    ['.', '.', '.', '.', '.'],
    ['.', 'N', '.', '.', '.'],
    ['.', '^', '.', '.', '.'],
    ['.', '.', '.', '.', '.']
] # 1

grid12 = [
    ['^', '.', '.', '^'],
    ['.', '.', '.', 'W'],
    ['.', '.', '.', '.'],
    ['.', '.', '.', '.'],
    ['^', '.', '.', '^']
] # 1

# Larger cases:
grid13 = [
    ['.', '.', 'W', 'W', '.', 'W', '.', 'W', 'W'],
    ['.', '.', 'W', '.', 'W', '.', 'W', '^', '.'],
    ['W', '.', '.', 'W', '<', 'W', '.', 'N', '.'],
    ['.', '.', '.', 'W', '.', '.', '.', '.', 'W'],
    ['.', '.', 'W', '.', 'W', '.', '.', '.', 'N'],
    ['.', '.', 'W', 'W', 'W', '>', '.', '.', '^'],
    ['<', '.', 'v', '.', '^', '.', '<', '.', 'W'],
    ['.', 'W', '.', '.', 'W', '.', '.', '.', '.'],
    ['.', '.', 'W', 'W', 'W', '.', '.', '.', '.']
]  # 3

grid14 = [
    ['.', 'W', '.', '>', '.', '.', '.', '<', 'W', '.'],
    ['.', 'W', 'W', '.', '^', '.', '.', '.', 'W', 'N'],
    ['.', 'v', '.', 'v', 'v', '.', '.', '<', '.', '.'],
    ['.', 'W', '.', '.', '.', '.', '.', '.', '.', '^']
]  # 4

grid15 = [
    ['.', '.', '^', '.', '.', '.', 'W'],
    ['>', 'v', 'W', '.', '.', '.', 'W'],
    ['.', '.', '.', '.', 'v', '.', '.'],
    ['.', '.', '.', '.', '.', 'W', '.'],
    ['.', '<', 'W', '.', '>', '.', 'N'],
    ['.', '.', '.', '.', '.', 'W', '.'],
    ['v', 'W', '.', '.', '.', 'v', '>'],
    ['.', 'W', 'W', 'N', 'W', 'N', '.']
]  # 5

grid16 = [
    ['.', '.', '.', '.'],
    ['.', '.', 'v', '.'],
    ['.', '>', 'N', '<'],
    ['.', '.', '^', '.']
] # 1

grid17 = [
    ['>', 'v'],
    ['^', '<'],
] # 4




def lasers(arr):
    found_laser=False
    for i,ele in enumerate(arr):
        if found_laser and ele == 'L':
            return True
        elif ele == 'W':
            found_laser=False
        elif ele == 'L':
            found_laser=True
    return False

puzzle1 = ['.', '.', '.', 'L', '.', '.', 'W', '.', 'L']
puzzle2 = ['.', '.', '.', 'L', '.', 'L', '.', '.', 'W', '.', 'L']
puzzle3 = ['L', 'L', 'W']
puzzle4 = ['W', 'L', 'L']
puzzle5 = ['.', '.', 'L', '.', '.', 'W', '.', '.', '.', '.']
puzzle6 = ['W']
puzzle7 = ['L', '.', 'L', '.', 'L', '.', 'W', '.', 'L']
puzzle8 = ['L', '.', 'W', '.', 'L', '.', 'L']

# ['W', 'L', '.', '.', '.']

# print(lasers(puzzle1))
# print(lasers(puzzle2))
# print(lasers(puzzle3))
# print(lasers(puzzle4))
# print(lasers(puzzle5))
# print(lasers(puzzle6))
# print(lasers(puzzle7))
# print(lasers(puzzle8))
