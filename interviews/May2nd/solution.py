counts = [
    "50,google.com",
    "60,yahoo.com",
    "10,yahoo.com",
    "1,wikipedia.org",
    "40,sports.yahoo.com",
    "300,yahoo.com",
    "2,wikipedia.org",
    "1,stackoverflow.com",
    "1,google.com"
];

def url_counts(arr):
    count={}
    for ele in arr:
        url = url_return(ele)
        count[url]=0
    for ele in arr:
        url = url_return(ele)
        clicks=click_return(ele)
        count[url]+=int(clicks)
    return count

def url_return(string):
    str=""
    foundComma=False
    for i in range(len(string)):
        if string[i]==",":
            foundComma=True
        if foundComma and string[i]!=",":
            str+=string[i]
    return str

def click_return(string):
  num=""
  for i in string:
    if i!=",":
      num+=i
    if i == ",":
      break
  return num

print(url_counts(counts))
