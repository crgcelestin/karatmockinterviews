const counts = [
    "50,google.com",
    "60,yahoo.com",
    "10,yahoo.com",
    "1,wikipedia.org",
    "40,sports.yahoo.com",
    "300,yahoo.com",
    "2,wikipedia.org",
    "1,stackoverflow.com",
    "1,google.com"
];

/*
  (1) iterate through the string
  (2) click count, first in string
  (2a) add characters in string to an empty string
  (3) use conditional, once we find a comma
    stop and then return string aggregate as integer
*/

/*
  (1) iterate array with string
  (2) intialize empty dictionary, starting after comma we want to take url part of string and make it a key in dictionary
  (3) make a conditional, if key === url, take sum and add it to current existing value

  - Data Structure Example
    After summing:
    50
    1
    ->
    {
        'google.com':51,
        ...
    }
*/

function url_counts(arr) {
    let count = {}
    for (let ele of arr) {
        const url = UrlReturn(ele)
        count[url] = 0
    }
    for (let ele of arr) {
        const url = UrlReturn(ele)
        if (count[url] !== undefined) {
            count[url] += parseInt(ele)
        }
    }
    return count
}

function UrlReturn(string) {
    let str = ""
    let foundComma = false
    for (let i = 0; i < string.length; i++) {
        if (string[i] === ",") {
            foundComma = true
        }
        if (foundComma && string[i] !== ",") {
            str += string[i]
        }
    }
    return str
}

console.log(url_counts(counts))

//Prior functions to get to resolute solution
function sum_clicks(arr) {
    let sum_arr = []
    let sum = 0
    for (let i = 0; i < arr.length; i++) {
        sum_arr.push(parseInt(UrlClick(arr[i])))
    }
    for (let ele of sum_arr) {
        sum += ele
    }
    return sum
}

function UrlClick(string) {
    let sum = ""
    for (let i = 0; i < string.length; i++) {
        if (string[i] !== ",") {
            sum += string[i]
        }
        if (string[i] === ",") {
            break
        }
    }
    return (sum)
}



// console.log(UrlReturn("50,google.com"))



// console.log(sum_clicks([
//     "50,google.com",
//     "60,yahoo.com",
//     "10,yahoo.com",
//     "1,wikipedia.org",
//     "40,sports.yahoo.com",
//     "300,yahoo.com",
//     "2,wikipedia.org",
//     "1,stackoverflow.com",
//     "1,google.com"
// ]))
