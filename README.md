# KaratMockInterviews (__Brilliant Black Minds__)
- Mock Interviews starting from April conducted on the Brilliant BlackMinds-Karat Platform
- Each Interview getting more complex with a chance to interview at a Partner Company

| Date | Prompt | Java Script Solution| Python Solution |
|----| ---- | ---- | --- |
|April 4th | [April 4th Prompt](April4th/april4th.md)|[Solution](April4th/solution.js) | [Solution](April4th/solution.py) |
|April 18th | [April 18th Prompt](April18th/april18th.md)| [Solution](April18th/solution.js) | -  |
|April 26th | [April 26th Prompt](April26th/april26th.md) | [Solution](April26th/solutions.js) | -  |
May 2nd | [May 2nd Prompt](May2nd/may2nd.md) | [Solution](May2nd/solution.js) | [Solution](May2nd/solution.py)
May 9th | [May 9th Prompt](May9th/may9th.md) | [Solution](May9th/solution.js)| [Solution](May9th/solution.py) |
May 19th | [May 19th Prompt](May19th/may19th.md) | [Solution](May19th/solution.js) | [Solution](May19th/solution.py)|
May 23rd | [May 23rd Prompt](May23rd/may23rd.md) | [Solution](May23rd/solution.js)  | [Solution](May23rd/solution.py)
May 29th | [May 29th Prompt](May29th/may29th.md) | - | [Solution](May29th/solution.py) |
June 4th | [June 4th Prompt](June4th/june4th.md)| - | [Solution](June4th/solution.py) |
June 9th | [June 9th Prompt](June9th/june9th.md) | - | [Solution](June9th/solution.py)
